<?php

/**
 * @file
 * Contains time_box.page.inc.
 *
 * Page callback for Time box entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Time box templates.
 *
 * Default template: time_box.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_time_box(array &$variables) {
  // Fetch TimeBox Entity Object.
  $time_box = $variables['elements']['#time_box'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
