<?php

/**
 * @file
 * Contains test_step.page.inc.
 *
 * Page callback for Test step entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Test step templates.
 *
 * Default template: test_step.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_test_step(array &$variables) {
  // Fetch TestStep Entity Object.
  $test_step = $variables['elements']['#test_step'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
