<?php

/**
 * @file
 * Contains test_run.page.inc.
 *
 * Page callback for Test run entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Test run templates.
 *
 * Default template: test_run.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_test_run(array &$variables) {
  // Fetch TestRun Entity Object.
  $test_run = $variables['elements']['#test_run'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
