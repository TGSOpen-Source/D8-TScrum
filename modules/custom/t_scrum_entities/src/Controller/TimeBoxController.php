<?php

namespace Drupal\t_scrum_entities\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\t_scrum_entities\Entity\TimeBoxInterface;

/**
 * Class TimeBoxController.
 *
 *  Returns responses for Time box routes.
 */
class TimeBoxController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Time box  revision.
   *
   * @param int $time_box_revision
   *   The Time box  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($time_box_revision) {
    $time_box = $this->entityManager()->getStorage('time_box')->loadRevision($time_box_revision);
    $view_builder = $this->entityManager()->getViewBuilder('time_box');

    return $view_builder->view($time_box);
  }

  /**
   * Page title callback for a Time box  revision.
   *
   * @param int $time_box_revision
   *   The Time box  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($time_box_revision) {
    $time_box = $this->entityManager()->getStorage('time_box')->loadRevision($time_box_revision);
    return $this->t('Revision of %title from %date', ['%title' => $time_box->label(), '%date' => format_date($time_box->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Time box .
   *
   * @param \Drupal\t_scrum_entities\Entity\TimeBoxInterface $time_box
   *   A Time box  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(TimeBoxInterface $time_box) {
    $account = $this->currentUser();
    $langcode = $time_box->language()->getId();
    $langname = $time_box->language()->getName();
    $languages = $time_box->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $time_box_storage = $this->entityManager()->getStorage('time_box');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $time_box->label()]) : $this->t('Revisions for %title', ['%title' => $time_box->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all time box revisions") || $account->hasPermission('administer time box entities')));
    $delete_permission = (($account->hasPermission("delete all time box revisions") || $account->hasPermission('administer time box entities')));

    $rows = [];

    $vids = $time_box_storage->revisionIds($time_box);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\t_scrum_entities\TimeBoxInterface $revision */
      $revision = $time_box_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $time_box->getRevisionId()) {
          $link = $this->l($date, new Url('entity.time_box.revision', ['time_box' => $time_box->id(), 'time_box_revision' => $vid]));
        }
        else {
          $link = $time_box->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.time_box.translation_revert', ['time_box' => $time_box->id(), 'time_box_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.time_box.revision_revert', ['time_box' => $time_box->id(), 'time_box_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.time_box.revision_delete', ['time_box' => $time_box->id(), 'time_box_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['time_box_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
