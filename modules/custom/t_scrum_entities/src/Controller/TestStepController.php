<?php

namespace Drupal\t_scrum_entities\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\t_scrum_entities\Entity\TestStepInterface;

/**
 * Class TestStepController.
 *
 *  Returns responses for Test step routes.
 */
class TestStepController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Test step  revision.
   *
   * @param int $test_step_revision
   *   The Test step  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($test_step_revision) {
    $test_step = $this->entityManager()->getStorage('test_step')->loadRevision($test_step_revision);
    $view_builder = $this->entityManager()->getViewBuilder('test_step');

    return $view_builder->view($test_step);
  }

  /**
   * Page title callback for a Test step  revision.
   *
   * @param int $test_step_revision
   *   The Test step  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($test_step_revision) {
    $test_step = $this->entityManager()->getStorage('test_step')->loadRevision($test_step_revision);
    return $this->t('Revision of %title from %date', ['%title' => $test_step->label(), '%date' => format_date($test_step->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Test step .
   *
   * @param \Drupal\t_scrum_entities\Entity\TestStepInterface $test_step
   *   A Test step  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(TestStepInterface $test_step) {
    $account = $this->currentUser();
    $langcode = $test_step->language()->getId();
    $langname = $test_step->language()->getName();
    $languages = $test_step->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $test_step_storage = $this->entityManager()->getStorage('test_step');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $test_step->label()]) : $this->t('Revisions for %title', ['%title' => $test_step->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all test step revisions") || $account->hasPermission('administer test step entities')));
    $delete_permission = (($account->hasPermission("delete all test step revisions") || $account->hasPermission('administer test step entities')));

    $rows = [];

    $vids = $test_step_storage->revisionIds($test_step);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\t_scrum_entities\TestStepInterface $revision */
      $revision = $test_step_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $test_step->getRevisionId()) {
          $link = $this->l($date, new Url('entity.test_step.revision', ['test_step' => $test_step->id(), 'test_step_revision' => $vid]));
        }
        else {
          $link = $test_step->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.test_step.translation_revert', ['test_step' => $test_step->id(), 'test_step_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.test_step.revision_revert', ['test_step' => $test_step->id(), 'test_step_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.test_step.revision_delete', ['test_step' => $test_step->id(), 'test_step_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['test_step_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
