<?php

namespace Drupal\t_scrum_entities\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\t_scrum_entities\Entity\TestRunInterface;

/**
 * Class TestRunController.
 *
 *  Returns responses for Test run routes.
 */
class TestRunController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Test run  revision.
   *
   * @param int $test_run_revision
   *   The Test run  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($test_run_revision) {
    $test_run = $this->entityManager()->getStorage('test_run')->loadRevision($test_run_revision);
    $view_builder = $this->entityManager()->getViewBuilder('test_run');

    return $view_builder->view($test_run);
  }

  /**
   * Page title callback for a Test run  revision.
   *
   * @param int $test_run_revision
   *   The Test run  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($test_run_revision) {
    $test_run = $this->entityManager()->getStorage('test_run')->loadRevision($test_run_revision);
    return $this->t('Revision of %title from %date', ['%title' => $test_run->label(), '%date' => format_date($test_run->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Test run .
   *
   * @param \Drupal\t_scrum_entities\Entity\TestRunInterface $test_run
   *   A Test run  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(TestRunInterface $test_run) {
    $account = $this->currentUser();
    $langcode = $test_run->language()->getId();
    $langname = $test_run->language()->getName();
    $languages = $test_run->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $test_run_storage = $this->entityManager()->getStorage('test_run');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $test_run->label()]) : $this->t('Revisions for %title', ['%title' => $test_run->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all test run revisions") || $account->hasPermission('administer test run entities')));
    $delete_permission = (($account->hasPermission("delete all test run revisions") || $account->hasPermission('administer test run entities')));

    $rows = [];

    $vids = $test_run_storage->revisionIds($test_run);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\t_scrum_entities\TestRunInterface $revision */
      $revision = $test_run_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $test_run->getRevisionId()) {
          $link = $this->l($date, new Url('entity.test_run.revision', ['test_run' => $test_run->id(), 'test_run_revision' => $vid]));
        }
        else {
          $link = $test_run->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.test_run.translation_revert', ['test_run' => $test_run->id(), 'test_run_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.test_run.revision_revert', ['test_run' => $test_run->id(), 'test_run_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.test_run.revision_delete', ['test_run' => $test_run->id(), 'test_run_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['test_run_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
