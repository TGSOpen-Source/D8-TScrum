<?php

namespace Drupal\t_scrum_entities;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Time box entities.
 *
 * @ingroup t_scrum_entities
 */
class TimeBoxListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Time box ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\t_scrum_entities\Entity\TimeBox */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.time_box.edit_form',
      ['time_box' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
