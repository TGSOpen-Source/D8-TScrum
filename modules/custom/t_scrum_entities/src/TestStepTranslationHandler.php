<?php

namespace Drupal\t_scrum_entities;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for test_step.
 */
class TestStepTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
