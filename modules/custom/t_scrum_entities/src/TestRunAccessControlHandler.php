<?php

namespace Drupal\t_scrum_entities;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Test run entity.
 *
 * @see \Drupal\t_scrum_entities\Entity\TestRun.
 */
class TestRunAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\t_scrum_entities\Entity\TestRunInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished test run entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published test run entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit test run entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete test run entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add test run entities');
  }

}
