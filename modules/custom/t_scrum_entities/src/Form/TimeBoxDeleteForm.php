<?php

namespace Drupal\t_scrum_entities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Time box entities.
 *
 * @ingroup t_scrum_entities
 */
class TimeBoxDeleteForm extends ContentEntityDeleteForm {


}
