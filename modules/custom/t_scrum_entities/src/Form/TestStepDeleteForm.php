<?php

namespace Drupal\t_scrum_entities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Test step entities.
 *
 * @ingroup t_scrum_entities
 */
class TestStepDeleteForm extends ContentEntityDeleteForm {


}
