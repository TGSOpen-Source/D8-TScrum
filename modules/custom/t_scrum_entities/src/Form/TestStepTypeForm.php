<?php

namespace Drupal\t_scrum_entities\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class TestStepTypeForm.
 */
class TestStepTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $test_step_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $test_step_type->label(),
      '#description' => $this->t("Label for the Test step type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $test_step_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\t_scrum_entities\Entity\TestStepType::load',
      ],
      '#disabled' => !$test_step_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $test_step_type = $this->entity;
    $status = $test_step_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Test step type.', [
          '%label' => $test_step_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Test step type.', [
          '%label' => $test_step_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($test_step_type->toUrl('collection'));
  }

}
