<?php

namespace Drupal\t_scrum_entities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Test run entities.
 *
 * @ingroup t_scrum_entities
 */
class TestRunDeleteForm extends ContentEntityDeleteForm {


}
