<?php

namespace Drupal\t_scrum_entities\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class TimeBoxTypeForm.
 */
class TimeBoxTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $time_box_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $time_box_type->label(),
      '#description' => $this->t("Label for the Time box type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $time_box_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\t_scrum_entities\Entity\TimeBoxType::load',
      ],
      '#disabled' => !$time_box_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $time_box_type = $this->entity;
    $status = $time_box_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Time box type.', [
          '%label' => $time_box_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Time box type.', [
          '%label' => $time_box_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($time_box_type->toUrl('collection'));
  }

}
