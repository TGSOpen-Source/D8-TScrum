<?php

namespace Drupal\t_scrum_entities;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\t_scrum_entities\Entity\TimeBoxInterface;

/**
 * Defines the storage handler class for Time box entities.
 *
 * This extends the base storage class, adding required special handling for
 * Time box entities.
 *
 * @ingroup t_scrum_entities
 */
interface TimeBoxStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Time box revision IDs for a specific Time box.
   *
   * @param \Drupal\t_scrum_entities\Entity\TimeBoxInterface $entity
   *   The Time box entity.
   *
   * @return int[]
   *   Time box revision IDs (in ascending order).
   */
  public function revisionIds(TimeBoxInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Time box author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Time box revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\t_scrum_entities\Entity\TimeBoxInterface $entity
   *   The Time box entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(TimeBoxInterface $entity);

  /**
   * Unsets the language for all Time box with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
