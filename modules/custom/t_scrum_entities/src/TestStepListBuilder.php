<?php

namespace Drupal\t_scrum_entities;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Test step entities.
 *
 * @ingroup t_scrum_entities
 */
class TestStepListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Test step ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\t_scrum_entities\Entity\TestStep */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.test_step.edit_form',
      ['test_step' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
