<?php

namespace Drupal\t_scrum_entities;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Test step entity.
 *
 * @see \Drupal\t_scrum_entities\Entity\TestStep.
 */
class TestStepAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\t_scrum_entities\Entity\TestStepInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished test step entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published test step entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit test step entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete test step entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add test step entities');
  }

}
