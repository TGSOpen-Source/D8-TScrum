<?php

namespace Drupal\t_scrum_entities\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Test step type entities.
 */
interface TestStepTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
