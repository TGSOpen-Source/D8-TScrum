<?php

namespace Drupal\t_scrum_entities\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Test run type entities.
 */
interface TestRunTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
