<?php

namespace Drupal\t_scrum_entities\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Time box type entities.
 */
interface TimeBoxTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
