<?php

namespace Drupal\t_scrum_entities\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Test run type entity.
 *
 * @ConfigEntityType(
 *   id = "test_run_type",
 *   label = @Translation("Test run type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\t_scrum_entities\TestRunTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\t_scrum_entities\Form\TestRunTypeForm",
 *       "edit" = "Drupal\t_scrum_entities\Form\TestRunTypeForm",
 *       "delete" = "Drupal\t_scrum_entities\Form\TestRunTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\t_scrum_entities\TestRunTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "test_run_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "test_run",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/t-scrum-entities/test_run_type/{test_run_type}",
 *     "add-form" = "/admin/structure/t-scrum-entities/test_run_type/add",
 *     "edit-form" = "/admin/structure/t-scrum-entities/test_run_type/{test_run_type}/edit",
 *     "delete-form" = "/admin/structure/t-scrum-entities/test_run_type/{test_run_type}/delete",
 *     "collection" = "/admin/structure/t-scrum-entities/test_run_type"
 *   }
 * )
 */
class TestRunType extends ConfigEntityBundleBase implements TestRunTypeInterface {

  /**
   * The Test run type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Test run type label.
   *
   * @var string
   */
  protected $label;

}
