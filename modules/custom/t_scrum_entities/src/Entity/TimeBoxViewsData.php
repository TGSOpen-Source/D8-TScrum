<?php

namespace Drupal\t_scrum_entities\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Time box entities.
 */
class TimeBoxViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
