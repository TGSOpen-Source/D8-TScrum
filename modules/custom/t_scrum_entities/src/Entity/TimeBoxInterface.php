<?php

namespace Drupal\t_scrum_entities\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Time box entities.
 *
 * @ingroup t_scrum_entities
 */
interface TimeBoxInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Time box name.
   *
   * @return string
   *   Name of the Time box.
   */
  public function getName();

  /**
   * Sets the Time box name.
   *
   * @param string $name
   *   The Time box name.
   *
   * @return \Drupal\t_scrum_entities\Entity\TimeBoxInterface
   *   The called Time box entity.
   */
  public function setName($name);

  /**
   * Gets the Time box creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Time box.
   */
  public function getCreatedTime();

  /**
   * Sets the Time box creation timestamp.
   *
   * @param int $timestamp
   *   The Time box creation timestamp.
   *
   * @return \Drupal\t_scrum_entities\Entity\TimeBoxInterface
   *   The called Time box entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Time box published status indicator.
   *
   * Unpublished Time box are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Time box is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Time box.
   *
   * @param bool $published
   *   TRUE to set this Time box to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\t_scrum_entities\Entity\TimeBoxInterface
   *   The called Time box entity.
   */
  public function setPublished($published);

  /**
   * Gets the Time box revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Time box revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\t_scrum_entities\Entity\TimeBoxInterface
   *   The called Time box entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Time box revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Time box revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\t_scrum_entities\Entity\TimeBoxInterface
   *   The called Time box entity.
   */
  public function setRevisionUserId($uid);

}
