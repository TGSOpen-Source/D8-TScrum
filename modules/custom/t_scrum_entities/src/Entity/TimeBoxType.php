<?php

namespace Drupal\t_scrum_entities\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Time box type entity.
 *
 * @ConfigEntityType(
 *   id = "time_box_type",
 *   label = @Translation("Time box type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\t_scrum_entities\TimeBoxTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\t_scrum_entities\Form\TimeBoxTypeForm",
 *       "edit" = "Drupal\t_scrum_entities\Form\TimeBoxTypeForm",
 *       "delete" = "Drupal\t_scrum_entities\Form\TimeBoxTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\t_scrum_entities\TimeBoxTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "time_box_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "time_box",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/t-scrum-entities/time_box_type/{time_box_type}",
 *     "add-form" = "/admin/structure/t-scrum-entities/time_box_type/add",
 *     "edit-form" = "/admin/structure/t-scrum-entities/time_box_type/{time_box_type}/edit",
 *     "delete-form" = "/admin/structure/t-scrum-entities/time_box_type/{time_box_type}/delete",
 *     "collection" = "/admin/structure/t-scrum-entities/time_box_type"
 *   }
 * )
 */
class TimeBoxType extends ConfigEntityBundleBase implements TimeBoxTypeInterface {

  /**
   * The Time box type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Time box type label.
   *
   * @var string
   */
  protected $label;

}
