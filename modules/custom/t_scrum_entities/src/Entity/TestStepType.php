<?php

namespace Drupal\t_scrum_entities\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Test step type entity.
 *
 * @ConfigEntityType(
 *   id = "test_step_type",
 *   label = @Translation("Test step type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\t_scrum_entities\TestStepTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\t_scrum_entities\Form\TestStepTypeForm",
 *       "edit" = "Drupal\t_scrum_entities\Form\TestStepTypeForm",
 *       "delete" = "Drupal\t_scrum_entities\Form\TestStepTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\t_scrum_entities\TestStepTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "test_step_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "test_step",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/t-scrum-entities/test_step_type/{test_step_type}",
 *     "add-form" = "/admin/structure/t-scrum-entities/test_step_type/add",
 *     "edit-form" = "/admin/structure/t-scrum-entities/test_step_type/{test_step_type}/edit",
 *     "delete-form" = "/admin/structure/t-scrum-entities/test_step_type/{test_step_type}/delete",
 *     "collection" = "/admin/structure/t-scrum-entities/test_step_type"
 *   }
 * )
 */
class TestStepType extends ConfigEntityBundleBase implements TestStepTypeInterface {

  /**
   * The Test step type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Test step type label.
   *
   * @var string
   */
  protected $label;

}
