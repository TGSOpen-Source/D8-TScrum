<?php

namespace Drupal\t_scrum_entities\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Test step entities.
 *
 * @ingroup t_scrum_entities
 */
interface TestStepInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Test step name.
   *
   * @return string
   *   Name of the Test step.
   */
  public function getName();

  /**
   * Sets the Test step name.
   *
   * @param string $name
   *   The Test step name.
   *
   * @return \Drupal\t_scrum_entities\Entity\TestStepInterface
   *   The called Test step entity.
   */
  public function setName($name);

  /**
   * Gets the Test step creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Test step.
   */
  public function getCreatedTime();

  /**
   * Sets the Test step creation timestamp.
   *
   * @param int $timestamp
   *   The Test step creation timestamp.
   *
   * @return \Drupal\t_scrum_entities\Entity\TestStepInterface
   *   The called Test step entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Test step published status indicator.
   *
   * Unpublished Test step are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Test step is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Test step.
   *
   * @param bool $published
   *   TRUE to set this Test step to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\t_scrum_entities\Entity\TestStepInterface
   *   The called Test step entity.
   */
  public function setPublished($published);

  /**
   * Gets the Test step revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Test step revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\t_scrum_entities\Entity\TestStepInterface
   *   The called Test step entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Test step revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Test step revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\t_scrum_entities\Entity\TestStepInterface
   *   The called Test step entity.
   */
  public function setRevisionUserId($uid);

}
