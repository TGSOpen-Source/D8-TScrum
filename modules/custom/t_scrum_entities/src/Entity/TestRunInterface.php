<?php

namespace Drupal\t_scrum_entities\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Test run entities.
 *
 * @ingroup t_scrum_entities
 */
interface TestRunInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Test run name.
   *
   * @return string
   *   Name of the Test run.
   */
  public function getName();

  /**
   * Sets the Test run name.
   *
   * @param string $name
   *   The Test run name.
   *
   * @return \Drupal\t_scrum_entities\Entity\TestRunInterface
   *   The called Test run entity.
   */
  public function setName($name);

  /**
   * Gets the Test run creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Test run.
   */
  public function getCreatedTime();

  /**
   * Sets the Test run creation timestamp.
   *
   * @param int $timestamp
   *   The Test run creation timestamp.
   *
   * @return \Drupal\t_scrum_entities\Entity\TestRunInterface
   *   The called Test run entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Test run published status indicator.
   *
   * Unpublished Test run are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Test run is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Test run.
   *
   * @param bool $published
   *   TRUE to set this Test run to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\t_scrum_entities\Entity\TestRunInterface
   *   The called Test run entity.
   */
  public function setPublished($published);

  /**
   * Gets the Test run revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Test run revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\t_scrum_entities\Entity\TestRunInterface
   *   The called Test run entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Test run revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Test run revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\t_scrum_entities\Entity\TestRunInterface
   *   The called Test run entity.
   */
  public function setRevisionUserId($uid);

}
