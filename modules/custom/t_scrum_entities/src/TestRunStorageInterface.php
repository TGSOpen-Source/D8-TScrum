<?php

namespace Drupal\t_scrum_entities;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\t_scrum_entities\Entity\TestRunInterface;

/**
 * Defines the storage handler class for Test run entities.
 *
 * This extends the base storage class, adding required special handling for
 * Test run entities.
 *
 * @ingroup t_scrum_entities
 */
interface TestRunStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Test run revision IDs for a specific Test run.
   *
   * @param \Drupal\t_scrum_entities\Entity\TestRunInterface $entity
   *   The Test run entity.
   *
   * @return int[]
   *   Test run revision IDs (in ascending order).
   */
  public function revisionIds(TestRunInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Test run author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Test run revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\t_scrum_entities\Entity\TestRunInterface $entity
   *   The Test run entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(TestRunInterface $entity);

  /**
   * Unsets the language for all Test run with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
