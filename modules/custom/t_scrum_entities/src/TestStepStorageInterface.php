<?php

namespace Drupal\t_scrum_entities;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\t_scrum_entities\Entity\TestStepInterface;

/**
 * Defines the storage handler class for Test step entities.
 *
 * This extends the base storage class, adding required special handling for
 * Test step entities.
 *
 * @ingroup t_scrum_entities
 */
interface TestStepStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Test step revision IDs for a specific Test step.
   *
   * @param \Drupal\t_scrum_entities\Entity\TestStepInterface $entity
   *   The Test step entity.
   *
   * @return int[]
   *   Test step revision IDs (in ascending order).
   */
  public function revisionIds(TestStepInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Test step author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Test step revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\t_scrum_entities\Entity\TestStepInterface $entity
   *   The Test step entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(TestStepInterface $entity);

  /**
   * Unsets the language for all Test step with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
