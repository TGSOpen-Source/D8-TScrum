<?php

namespace Drupal\t_scrum_entities;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\t_scrum_entities\Entity\TimeBoxInterface;

/**
 * Defines the storage handler class for Time box entities.
 *
 * This extends the base storage class, adding required special handling for
 * Time box entities.
 *
 * @ingroup t_scrum_entities
 */
class TimeBoxStorage extends SqlContentEntityStorage implements TimeBoxStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(TimeBoxInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {time_box_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {time_box_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(TimeBoxInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {time_box_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('time_box_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
