<?php

namespace Drupal\t_scrum_entities;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Time box entity.
 *
 * @see \Drupal\t_scrum_entities\Entity\TimeBox.
 */
class TimeBoxAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\t_scrum_entities\Entity\TimeBoxInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished time box entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published time box entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit time box entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete time box entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add time box entities');
  }

}
